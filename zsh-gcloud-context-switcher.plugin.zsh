function gcloud_context_switcher() {
    while IFS='=' read -r name value context; do
        if [[ $name == 'GCLOUD_CONTEXT_SWITCHER_'* ]]; then
            if [[ $(pwd) =~ .*${value}.* ]]; then
                if [[ $(< $HOME/.config/gcloud/active_config) != "${context}" ]]; then
                    gcloud config configurations activate $context
                fi
            fi
        fi
    done < <(env)
}

chpwd_functions+=(gcloud_context_switcher)
